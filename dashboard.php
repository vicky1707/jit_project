<?php
require('dbconn.php');

    $sql = "SELECT * FROM users";
    $stmt = $conn->query($sql);
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // echo "<pre>";
    // print_r($users);
?>
<table border="1">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Age</th>
            <th>City</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($users as $rec){ ?>
            <tr>
                <td><?php echo $rec['id']; ?></td>
                <td><?php echo $rec['name']; ?></td>
                <td><?php echo $rec['age']; ?></td>
                <td><?php echo $rec['city']; ?></td>
                <td>
                    <a href="edit_form.php">Edit</a>
                    <a href="delete.php?id=<?php echo $rec['id']; ?>" onclick="return confirm('Are You Sure!')">Delete</a>
            

                </td>
            </tr>
        <?php } ?>
    </tbody>

</table>













